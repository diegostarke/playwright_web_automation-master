import { Page, expect } from '@playwright/test';


export class Homepage {
    constructor(private page: Page) {}

    public homeMenuButton = this.page.getByTestId('home');
    public cadastrarUsuariosMenuButton = this.page.getByTestId('cadastrar-usuarios');
    public listarUsuariosMenuButton = this.page.getByTestId('listar-usuarios');
    public cadastrarProdutosMenuButton = this.page.getByTestId('cadastrar-produtos');
    public listarProdutosMenuButton = this.page.getByTestId('listar-produtos');
    public linkRelatoriosMenuButton = this.page.getByTestId('link-relatorios');
    public logoutButton = this.page.getByTestId('logout');

    public cadastrarUsuariosButton = this.page.getByTestId('cadastrarUsuarios');
    public listarUsuariosButton = this.page.getByTestId('listarUsuarios');
    public cadastrarProdutosButton = this.page.getByTestId('cadastrarProdutos');
    public listarProdutosButton = this.page.getByTestId('listarProdutos');
    public linkRelatoriosButton = this.page.getByTestId('relatorios');

    //função para checar elementos
    async checkMenu() {
        await expect(this.homeMenuButton).toBeVisible();
        await expect(this.cadastrarUsuariosMenuButton).toBeVisible();
        await expect(this.listarUsuariosMenuButton).toBeVisible();
        await expect(this.cadastrarProdutosMenuButton).toBeVisible();
        await expect(this.linkRelatoriosMenuButton).toBeVisible();
        await expect(this.logoutButton).toBeVisible();
    }
    //função para checar buttons
    async checkButtons() {
        await expect(this.cadastrarUsuariosButton).toBeVisible();
        await expect(this.listarUsuariosButton).toBeVisible();
        await expect(this.cadastrarProdutosButton).toBeVisible();
        await expect(this.listarProdutosButton).toBeVisible();
        await expect(this.linkRelatoriosButton).toBeVisible();
    }
}