import { Page, expect } from '@playwright/test';


export class RegisterUserPage {
    constructor(private page: Page) {}

    public nameField = this.page.getByTestId('nome');
    public emailField = this.page.getByTestId('email');
    public passwordField = this.page.getByTestId('password');
    public administratorCheckbox = this.page.getByTestId('checkbox');
    public registerButton = this.page.getByTestId('cadastrarUsuario');
    public logoutButton = this.page.getByTestId('logout');

    async checkForm() {
        await expect(this.nameField).toBeVisible();
        await expect(this.emailField).toBeVisible();
        await expect(this.passwordField).toBeVisible();
        await expect(this.administratorCheckbox).toBeVisible();
        await expect(this.registerButton).toBeVisible();
        await expect(this.logoutButton).toBeVisible();
    }
}