import { Page, expect } from '@playwright/test';


export class LoginPage {
    constructor(private page: Page) {}

    public emailField = this.page.getByTestId('email');
    public passwordField = this.page.getByTestId('senha');
    public enterButton = this.page.getByTestId('entrar');
    public logoutButton = this.page.getByTestId('logout');

    async navigate() {
        await this.page.goto('/');
    }
    //função login
    async login(email: string, password: string) {
        await this.emailField.fill(email);
        await this.passwordField.fill(password);
        await this.enterButton.click();
    }

    async checkInvalidLogin() {
        await expect(this.page.getByText('Email e/ou senha inválidos')).toBeVisible();
    }
}