import { test, expect } from '@playwright/test';
import { LoginPage } from '../pages/LoginPage'
import { Homepage } from '../pages/HomePage';

//Login para verificação com credenciais válidas
test('Login in Serverest page with valid credentials', async ({ page }) => {
    const loginPage = new LoginPage(page);
    const homePage = new Homepage(page);
    await loginPage.navigate();
    await loginPage.login('diego.starke@gmail.com', 'admin123');

    await expect(homePage.logoutButton).toBeVisible();
});

//Login para verificação de campo obrigatório
test('Login in Serverest page without entering an email (mandatory field)', async ({ page }) => {
    const loginPage = new LoginPage(page);
    await loginPage.navigate();
    await loginPage.login('diegostarke', 'admin123');

    const type = await loginPage.emailField.getAttribute('type');
    expect(type).toBe('email');
});
//Login para verificação de credencial inválida
test('Login in Serverest page with invalid credentials', async ({ page }) => {
    const loginPage = new LoginPage(page);
    await loginPage.navigate();
    await loginPage.login('diego.stakre@gmail.com', 'admin1234');

    await loginPage.checkInvalidLogin();
});