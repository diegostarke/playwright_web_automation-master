import { test, expect } from '@playwright/test';
import { LoginPage } from '../pages/LoginPage'
import { Homepage } from '../pages/HomePage';
import { RegisterUserPage } from '../pages/RegisterUserPage';


test('Todos os itens do menu e botões são visualizados', async ({ page }) => {
    const loginPage = new LoginPage(page);
    const homePage = new Homepage(page);
    await loginPage.navigate();
    await loginPage.login('diego.starke@gmail.com', 'admin123');

    await homePage.checkMenu();
    await homePage.checkButtons();
});

test('Clicando no item Cadastrar Usuários no menu, redireciona para o cadastro de usuários', async ({ page }) => {
    const loginPage = new LoginPage(page);
    const homePage = new Homepage(page);
    const registerUserPage = new RegisterUserPage(page);
    await loginPage.navigate();
    await loginPage.login('diego.starke@gmail.com', 'admin123');
    await homePage.cadastrarUsuariosMenuButton.click();
    
    await registerUserPage.checkForm();
});